#################################################
# Project configuration
#################################################
PROJ=spiw32

OBJS=src/ctxsw.o src/ctxsw_backend.o \
	src/host.o src/ldt.o src/main.o \
	src/patch.o src/spi.o

DEFFILE=src/$(PROJ).def

#################################################
# Compiler/tool configuration
#################################################
BUILDDIR?=build/
DEBUG?=true

RM:=rm -rf

LFLAGS+=-MAP:FULL
LFLAGS+=-NOLOGO -ONERROR:NOEXE
LFLAGS+=-REORDERSEGMENTS -PACKCODE -PACKDATA
CFLAGS+=-Ab -j0 -p -r
CFLAGS+=-Isrc -I.
CFLAGS+=-mn -Nc -WA -5
CFLAGS+=-wx
SFLAGS+=-nologo
SFLAGS+=-Isrc -I.
SFLAGS+=-w
RESFLAGS+=-32 -Isrc -I.
LIBS+=

ifeq ($(DEBUG),true)
RESFLAGS+=-D
DEFS+=-DDEBUG=1
#CFLAGS+=-o+speed
LFLAGS+=-DEBUG
else
CFLAGS+=-o+speed
LFLAGS+=-EXEPACK
endif
CFLAGS+=$(DEFS)
SFLAGS+=$(DEFS)
RESFLAGS+=$(DEFS)

EXE?=.exe
TARGET=$(BUILDDIR)$(PROJ)$(EXE)
RESOBJ?=res/$(PROJ).res
DISTTARGET:=distrib/$(PROJ)/$(PROJ)$(EXE)

OBJS:=$(foreach x,$(OBJS),$(BUILDDIR)$x)
RESOBJ:=$(BUILDDIR)$(RESOBJ)
LINKTMP:=linktmp.txt

#################################################
# PHONY list
#################################################
.PHONY: all clean dist build run
# Clear implicit rules and define our own
.SUFFIXES:
.SUFFIXES: .c .rc

#################################################
# Generic rules (project independent)
#################################################
all: build
clean:
	$(RM) $(OBJS) $(RESOBJ) $(RESOBJ).h $(TARGET) $(TARGET).map $(LINKTMP)
dist: build
	tar czf distrib/$(PROJ).tar.gz -C distrib/ $(PROJ)/
build: $(DISTTARGET)
run: build
	$(DISTTARGET) $(dir $(DISTTARGET))

$(DISTTARGET): $(TARGET) Makefile
	@mkdir -p $(dir $@)
	cp $< $@

$(TARGET): $(OBJS) $(DEFFILE) $(RESOBJ) Makefile
	@mkdir -p $(dir $@)
	echo "$(OBJS)" >$(LINKTMP)
	echo "$@" >>$(LINKTMP)
	echo "$@.map" >>$(LINKTMP)
	echo "$(LIBS)" >>$(LINKTMP)
	echo "$(DEFFILE)" >>$(LINKTMP)
	echo "$(RESOBJ)" >>$(LINKTMP)
	sed -i -e 's/\//\\/g' $(LINKTMP)
	optlink $(LFLAGS) "@$(LINKTMP)"
	$(RM) $(LINKTMP)

#################################################
# File build rules
#################################################
$(BUILDDIR)src/%.o: src/%.c src/top.h Makefile
	@mkdir -p $(dir $@)
	dmc -c $(CFLAGS) $< -o$@
$(BUILDDIR)src/%.o: src/%.asm Makefile
	@mkdir -p $(dir $@)
	masm -c $(SFLAGS) -Fo$@ $<
$(BUILDDIR)res/%.res: res/%.rc Makefile
	@mkdir -p $(dir $@)
	rcc $< $(RESFLAGS) -o$@ -n$@.h

#################################################
# Resource file dependencies
#################################################
res/$(PROJ).rc: res/resource.h

#################################################
# Source file dependencies
#################################################
src/ctxsw.c: src/ctxsw.h src/host.h src/ldt.h src/spi_state.h
src/host.c: src/host.h src/ldt.h
src/ldt.c: src/host.h src/ldt.h
src/main.c: src/spi.h \
	res/resource.h
src/patch.c: src/patch.h src/spi.h src/spi_state.h
src/spi.c: src/ctxsw.h src/host.h src/ldt.h src/patch.h src/spi.h \
	src/spi_state.h

#################################################
# Header file dependencies
#################################################
src/patch.h: src/spi.h
src/spi_state.h: src/spi.h
