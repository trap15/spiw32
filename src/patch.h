/* SPIW32 - SPI for Windows 95
 *
 * Copyright (C) 2018 Alex "trap15" Marshall <trap15@raidenii.net>
 * All rights reserved. No warranty implied.
 */

/*! \file
 *
 * \brief Patching functionality.
 */

#ifndef PATCH_H_
#define PATCH_H_

#include "spi.h"

/*! \brief Applies game patches.
 *
 * \param[in] game  Game ID to apply patches for.
 */
void spi_patch(SPIGameID game);

#endif
