/* SPIW32 - SPI for Windows 95
 *
 * Copyright (C) 2018 Alex "trap15" Marshall <trap15@raidenii.net>
 * All rights reserved. No warranty implied.
 */

/*! \file
 *
 * \brief Main source file.
 */

#include "top.h"

#include "res/resource.h"
#include "spi.h"

HINSTANCE g_hInstance;

#if 1
#define DEBUG_GAMEID SPIGameID_rdft
#define DEBUG_GAMEPRG "rdft.bin"
#endif

static BOOL _load_game(SPIGameID gameid, const char *prgfn) {
  spi_cleanup();
  if(!spi_setup()) {
    MessageBox(NULL, "Cannot initialize", NULL, MB_SETFOREGROUND);
    PostQuitMessage(0);
    return FALSE;
  }
  spi_load_data(gameid, prgfn);
  return TRUE;
}

static LRESULT APIENTRY _main_wnd_proc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
  switch(uMsg) {
    case WM_COMMAND:
      switch(LOWORD(wParam)) {
        case IDM_FILE_OPEN:
          if(!_load_game(SPIGameID_rdft, "rdft.bin")) {
            MessageBox(0, "Can't load :(", "Hi", MB_SETFOREGROUND);
          }else{
            MessageBox(0, "Press OK", "Hi", MB_SETFOREGROUND);
          }
          break;
        case IDM_FILE_EXIT:
          MessageBox(NULL, "Thanks for playing!", "", MB_SETFOREGROUND);
          PostQuitMessage(0);
          break;
        default:
          break;
      }
      return 0;
    case WM_CLOSE:
      PostQuitMessage(0);
      break;
    default:
      break;
  }
  return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

static BOOL _create_window(int nCmdShow) {
  WNDCLASS wc;
  HWND hwnd;

  wc.style = 0;
  wc.lpfnWndProc = (WNDPROC)_main_wnd_proc;
  wc.cbClsExtra = 0;
  wc.cbWndExtra = 0;
  wc.hInstance = g_hInstance;
  wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wc.hCursor = LoadCursor(NULL, IDC_ARROW);
  wc.hbrBackground = (HBRUSH)COLOR_BACKGROUND;
  wc.lpszMenuName = MAKEINTRESOURCE(IDM_MAIN_MENU);
  wc.lpszClassName = "MainWClass";
  if(!RegisterClass(&wc)) {
    DBG("Couldn't register window class\n");
    return FALSE;
  }

  // TODO: Resource wrapper
  hwnd = CreateWindow("MainWClass", "Test of the Day",
                      WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT,
                      640, 480, NULL, NULL, g_hInstance,
                      NULL);
  if(hwnd == NULL) {
    DBG("Couldn't create main window\n");
    return FALSE;
  }
  ShowWindow(hwnd, nCmdShow);
  UpdateWindow(hwnd);
  return TRUE;
}

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                     LPSTR lpCmdLine, int nCmdShow) {
  g_hInstance = hInstance;
  if(!spi_setup()) {
    MessageBox(NULL, "Cannot initialize", NULL, MB_SETFOREGROUND);
    return 0;
  }
  if(!_create_window(nCmdShow)) {
    MessageBox(NULL, "Cannot create window", NULL, MB_SETFOREGROUND);
    return 0;
  }

#ifdef DEBUG_GAMEID
  if(!_load_game(DEBUG_GAMEID, DEBUG_GAMEPRG)) {
    MessageBox(0, "Can't load :(", "Hi", MB_SETFOREGROUND);
  }else{
    MessageBox(0, "Press OK", "Hi", MB_SETFOREGROUND);
  }
#endif

  MSG msg;
  while(GetMessage(&msg, NULL, 0, 0)) {
    TranslateMessage(&msg);
    DispatchMessage(&msg);
  }

  spi_cleanup();
  return msg.wParam;
}
