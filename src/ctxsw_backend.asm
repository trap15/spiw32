	.486

_DATA	SEGMENT PARA PUBLIC 'DATA' USE32
_DATA	ENDS

CGROUP	GROUP	_TEXT
DGROUP	GROUP	_DATA

	assume  cs:CGROUP, ds:DGROUP

_TEXT	SEGMENT WORD PUBLIC 'CODE' USE32
;//DWORD ctxsw_hostcall(void *host_func, DWORD call_arg);
	PUBLIC	_ctxsw_hostcall
_ctxsw_hostcall::
	RET

;/*! \brief Execute one function in SPI system context.
; *
; * \param[inout]  ctx   Context that will be used for execution.
; */
;void ctxsw_execute(SPIExecCtx *ctx)
	PUBLIC	_ctxsw_execute
_ctxsw_execute::
	push	ebp
	mov	ebp, esp
	push	ebx
	push	esi
	push	edi
	push	ds
	push	es
	push	fs
	push	gs

	mov	ebx, [ebp+8]
	; EBX = ctx
	mov	edx, [ebx+1Ch]	; r_esp
	mov	ax, [ebx+34h]	; r_ss
	mov	gs, ax
	mov	[ebx+40h], esp	; host_esp
	mov	cx, ss
	mov	[ebx+44h], cx	; host_ss
	mov	ss, ax
	mov	esp, edx
	; SS:ESP is now from guest
	mov	ecx, [ebx+08h]	; r_ecx
	mov	edx, [ebx+0Ch]	; r_edx
	mov	esi, [ebx+10h]	; r_esi
	mov	edi, [ebx+14h]	; r_edi
	mov	ebp, [ebx+18h]	; r_ebp
	mov	ax, [ebx+28h]	; r_es
	mov	es, ax
	mov	ax, [ebx+2Ch]	; r_fs
	mov	fs, ax
	mov	ax, [ebx+30h]	; r_gs
	mov	gs, ax
	mov	eax, [ebx+00h]	; r_eax
	push	ds	; push farptr to guest's ctx
	push	ebx
	push	_ctxsw_retshim	; Shim ourselves back after the function
	push	dword ptr [ebx+20h]	; r_cs ; push destination
	push	dword ptr [ebx+3Ch]	; r_eip
	; original args need to go here!
	push	dword ptr [ebx+04h]	; r_ebx
	push	dword ptr [ebx+24h]	; r_ds
	pop	ds
	pop	ebx
	retf

_ctxsw_retshim:
	xchg	ebx, [esp]	; swap ds:ebx with farptr to guest's ctx
	xchg	eax, 4[esp]
	push	ds
	mov	ds, ax
	pop	eax
	mov	[ebx+24h], eax	; r_ds saved
	mov	eax, [esp]
	mov	[ebx+04h], eax	; r_ebx saved from swapped
	mov	eax, [esp+4]
	add	esp, 8	; pop back the saved ctx ptr
	mov	[ebx+00h], eax	; r_eax saved from swapped
	mov	[ebx+08h], ecx	; r_ecx saved
	mov	[ebx+0Ch], edx	; r_edx saved
	mov	[ebx+10h], esi	; r_esi saved
	mov	[ebx+14h], edi	; r_edi saved
	mov	[ebx+18h], ebp	; r_ebp saved
	mov	[ebx+1Ch], esp	; r_esp saved
	mov	ax, es
	mov	[ebx+28h], ax	; r_es saved
	mov	ax, fs
	mov	[ebx+2Ch], ax	; r_fs saved
	mov	ax, gs
	mov	[ebx+30h], ax	; r_gs saved
	mov	ax, ss
	mov	[ebx+24h], ax	; r_ss saved
	mov	esp, [ebx+40h]
	mov	ax, [ebx+44h]
	mov	ss, ax
	; SS:ESP is back to host
	pop 	gs
	pop 	fs
	pop 	es
	pop 	ds
	pop 	edi
	pop 	esi
	pop 	ebx
	pop 	ebp
	ret

_TEXT	ENDS

	END
