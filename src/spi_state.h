/* SPIW32 - SPI for Windows 95
 *
 * Copyright (C) 2018 Alex "trap15" Marshall <trap15@raidenii.net>
 * All rights reserved. No warranty implied.
 */

/*! \file
 *
 * \brief General SPI state definition.
 */

#ifndef SPI_STATE_H_
#define SPI_STATE_H_

#include "spi.h"

#define SPI_RAM_OFFSET (0)
#define SPI_RAM_SIZE (256*1024)
#define SPI_RAM_END (SPI_RAM_OFFSET + SPI_RAM_SIZE)

#define SPI_HOLE_OFFSET (SPI_RAM_END)
#define SPI_HOLE_SIZE (SPI_HOLE_END - SPI_HOLE_OFFSET)
#define SPI_HOLE_END (SPI_ROM_OFFSET)

#define SPIHOLE_CALLSHIM_OFFSET (SPI_HOLE_OFFSET)
#define SPIHOLE_CALLSHIM_SIZE (6)
#define SPIHOLE_CALLSHIM_END (SPIHOLE_CALLSHIM_OFFSET + SPIHOLE_CALLSHIM_SIZE)
#define SPIHOLE_FREE_OFFSET (SPIHOLE_CALLSHIM_END)

#define SPI_ROM_OFFSET (2*1024*1024)
#define SPI_ROM_SIZE (2*1024*1024)

/*! \brief Total size of the SPI memory region, in bytes.
 *
 * The 2MB ROM area is placed at 2MB as usual, and the RAM is placed at 0 as
 * usual. However, since segments aren't _that_ powerful, the "hole" between
 * RAM and ROM must be allocated as well. Welp.
 */
#define SPI_MEM_SIZE (4*1024*1024)

/*! \brief Contains all (most) state for the SPI system.
 */
typedef struct {
  //! \brief Currently running game.
  SPIGameID game_id;

  //! \brief Pointer to the SPI memory base.
  BYTE *mem;

  //! \brief Offset to entrypoint for setup.
  DWORD entrypoint_setup;

  /*! \brief If set, the machine has run once.
   *
   * This is used to indicate whether game startup code needs to run.
   */
  BOOL has_run;

  /*! \brief Thread to run the CPU instructions.
   */
  HANDLE cpu_thread;

  /*! \brief Notifier for CPU task to begin execution.
   *
   * To run a frame of CPU execution, increment the semaphore with ReleaseSemaphore.
   */
  HANDLE cpu_wait_semaphore;
} SPIState;

//! \brief SPIState singleton.
extern SPIState g_spi;

#define RD32(addr) (*(DWORD*)(g_spi.mem + addr))
#define RD16(addr) (*(WORD*)(g_spi.mem + addr))
#define RD8(addr) (*(BYTE*)(g_spi.mem + addr))
#define WR32(addr,val) do { *(DWORD*)(g_spi.mem + addr) = val; } while(0)
#define WR16(addr,val) do { *(WORD*)(g_spi.mem + addr) = val; } while(0)
#define WR8(addr,val)  do { *(BYTE*)(g_spi.mem + addr) = val; } while(0)

#endif
