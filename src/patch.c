/* SPIW32 - SPI for Windows 95
 *
 * Copyright (C) 2018 Alex "trap15" Marshall <trap15@raidenii.net>
 * All rights reserved. No warranty implied.
 */

/*! \file
 *
 * \brief Patching functionality.
 */

#include "top.h"

#include "patch.h"
#include "spi.h"
#include "spi_state.h"

typedef struct {
  //! \brief Offset to entrypoint for setup.
  DWORD entrypoint_setup;
} GameDef;

static const GameDef s_gamedefs[SPIGameIDCount] = {
  [SPIGameID_rdft] = {
    .entrypoint_setup = 0x26B2FD,
  },
};

/*! \brief Apply patches for rdft.
 */
static void _spi_patch_rdft(void) {
}

/*! \brief Applies game patches.
 *
 * \param[in] game  Game ID to apply patches for.
 */
void spi_patch(SPIGameID game) {
  g_spi.entrypoint_setup = s_gamedefs[game].entrypoint_setup;

  switch(game) {
    case SPIGameID_rdft:
      _spi_patch_rdft();
      break;
    default:
      break;
  }
}
