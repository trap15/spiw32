/* SPIW32 - SPI for Windows 95
 *
 * Copyright (C) 2018 Alex "trap15" Marshall <trap15@raidenii.net>
 * All rights reserved. No warranty implied.
 */

/*! \file
 *
 * \brief General SPI system functions.
 */

#include "top.h"

#include "ctxsw.h"
#include "host.h"
#include "ldt.h"
#include "patch.h"
#include "spi.h"
#include "spi_state.h"

//! \brief SPIState singleton.
SPIState g_spi;

/*! \brief Thread to run the CPU core.
 */
static DWORD WINAPI _spi_cpu_thread(LPVOID param) {
  DBG("CPU thread arrived\n");
  for(;;) {
    DWORD res = WaitForSingleObject(g_spi.cpu_wait_semaphore, INFINITE);
    if(res != WAIT_OBJECT_0) {
      break;
    }
    // TODO: run single frame
    DBG("STEPPY!\n");
  }
}

/*! \brief Sets up anything required by the SPI system.
 *
 * \return TRUE if successful, FALSE otherwise.
 */
BOOL spi_setup(void) {
  host_setup();

  memset(&g_spi, 0, sizeof(g_spi));

  g_spi.mem = calloc(SPI_MEM_SIZE, 1);
  if(g_spi.mem == NULL) {
    DBG("calloc failed\n");
    goto _failed_mem;
  }

  if(!ldt_setup(g_spi.mem, SPI_MEM_SIZE)) {
    DBG("ldt failed\n");
    goto _failed_ldt;
  }
  // TODO: VirtualProtect the first 4K from access, since that's the HW periph
  // area, and we want to trap all those away anyways!

  g_spi.cpu_wait_semaphore = CreateSemaphore(NULL, 0, 16, NULL);
  if(g_spi.cpu_wait_semaphore == NULL) {
    DBG("cpu_wait_semaphore failed\n");
    goto _failed_sema;
  }

  DWORD dwThreadId = 0; // For some reason non-optional in win9x...
  g_spi.cpu_thread = CreateThread(NULL, 8192, _spi_cpu_thread, NULL, 0, &dwThreadId);
  if(g_spi.cpu_thread == NULL) {
    DBG("cpu_thread failed %08X\n", GetLastError());
    goto _failed_gamethr;
  }
  // TODO: Create video emu thread
  // TODO: Create audio emu thread?

  if(!ctxsw_init()) {
    DBG("ctxsw failed\n");
    goto _failed_ctxsw;
  }

  DBG("spi setup pass\n");
  return TRUE;

_failed_ctxsw:
  TerminateThread(g_spi.cpu_thread, 0);
  CloseHandle(g_spi.cpu_thread);
_failed_gamethr:
  CloseHandle(g_spi.cpu_wait_semaphore);
_failed_sema:
  ldt_cleanup();
_failed_ldt:
  free(g_spi.mem);
_failed_mem:
  return FALSE;
}

/*! \brief Cleans up resources used by the SPI system.
 */
void spi_cleanup(void) {
  TerminateThread(g_spi.cpu_thread, 0);
  CloseHandle(g_spi.cpu_thread);
  CloseHandle(g_spi.cpu_wait_semaphore);
  ldt_cleanup();
  free(g_spi.mem);
}

/*! \brief Loads ROM data.
 *
 * \param[in] game    ID of the game being loaded.
 * \param[in] prg_fn  Filename for the merged program ROM.
 *
 * \return TRUE if successful, FALSE otherwise.
 */
BOOL spi_load_data(SPIGameID game, const char *prg_fn) {
  FILE *fp = fopen(prg_fn, "rb");
  fread(g_spi.mem + SPI_ROM_OFFSET, SPI_ROM_SIZE, 1, fp);
  fclose(fp);

  spi_patch(game);
  return TRUE;
}

/*! \brief Execute a single frame.
 */
void spi_execute_frame(void) {
  if(!g_spi.has_run) {
//    spi_execute_subroutine(g_spi.entrypoint_setup);
    g_spi.has_run = TRUE;
  }
  ReleaseSemaphore(g_spi.cpu_wait_semaphore, 1, NULL);
}
