/* SPIW32 - SPI for Windows 95
 *
 * Copyright (C) 2018 Alex "trap15" Marshall <trap15@raidenii.net>
 * All rights reserved. No warranty implied.
 */

/*! \file
 *
 * \brief Context switch functionality.
 */

#ifndef CTXSW_H_
#define CTXSW_H_

/*! \brief The execution context for the SPI system.
 */
typedef struct {
  //    0x00   0x04   0x08   0x0C
  DWORD r_eax, r_ebx, r_ecx, r_edx; // 0x00
  DWORD r_esi, r_edi, r_ebp, r_esp; // 0x10
  DWORD r_cs,  r_ds,  r_es,  r_fs;  // 0x20
  DWORD r_gs,  r_ss,r_eflags,r_eip; // 0x30
  // Do not access the following values externally.
  DWORD host_esp, host_ss;          // 0x40
} SPIExecCtx;

/*! \brief Initialize the context switching code.
 *
 * Returns TRUE on success, FALSE on failure.
 */
BOOL ctxsw_init(void);

/*! \brief Initialize a context.
 *
 * \param[inout]  ctx   Context to initialize.
 */
void ctxsw_ctx_init(SPIExecCtx *ctx);

/*! \brief Set the stack for a context.
 *
 * \param[inout]  ctx   Context to assign stack to.
 * \param[in]     size  Number of bytes in the stack.
 * \param[inout]  ptr   Pointer to the base of the buffer to use. This is in host memory space.
 *                      If this is NULL, a buffer will be allocated. This _must_
 *                      be manually freed!
 */
void ctxsw_ctx_stack_set(SPIExecCtx *ctx, DWORD size, void *ptr);

/*! \brief Push one DWORD to a context's stack.
 *
 * \param[inout]  ctx     Context containing the stack to push to.
 * \param[in]     value   Value to push.
 */
void ctxsw_ctx_push(SPIExecCtx *ctx, DWORD value);

/*! \brief Execute one function in SPI system context.
 *
 * \param[inout]  ctx   Context that will be used for execution.
 */
void ctxsw_execute(SPIExecCtx *ctx);

/*! \brief Execute a host function from the VM.
 *
 * NOTE: This is a farcall, do not try to call it or write it in C.
 *
 * \param[in]   host_func   Pointer to the host function to run.
 *                          This is in host memory space.
 * \param[in]   call_arg    Pointer to the arguments in the guest's stack.
 *                          This is in guest memory space.
 */
DWORD ctxsw_hostcall(void *host_func, DWORD call_arg);

/* Example usage of this API to execute a function:
 *
 * SPIExecCtx ctx;
 * ctxsw_ctx_init(&ctx, [???]);
 * ctxsw_ctx_push(&ctx, args);
 * ctxsw_execute(&ctx, func);
 * [read results from ctx or its stack]
 */

/* Example usage of this API to call a host function from a hook:
 * NOTE: This clobbers ebx and esi, violating ABI!
 *
 * push <args>
 * push esp
 * push <host_func>
 * callf ds:[SPIHOLE_CALLSHIM_OFFSET]
 */

/*
CALLSHIM: farptr hostcs:ctxsw_hostcall
*/

#endif
