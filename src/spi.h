/* SPIW32 - SPI for Windows 95
 *
 * Copyright (C) 2018 Alex "trap15" Marshall <trap15@raidenii.net>
 * All rights reserved. No warranty implied.
 */

/*! \file
 *
 * \brief General SPI system functions.
 */

#ifndef SPI_H_
#define SPI_H_

/*! \brief List of games supported.
 */
typedef enum {
  //! \brief Raiden Fighters (World set)
  SPIGameID_rdft = 0,
  //! \brief Number of game IDs.
  SPIGameIDCount,
} SPIGameID;

/*! \brief Sets up anything required by the SPI system.
 *
 * \return TRUE if successful, FALSE otherwise.
 */
BOOL spi_setup(void);

/*! \brief Cleans up resources used by the SPI system.
 */
void spi_cleanup(void);

/*! \brief Loads ROM data.
 *
 * \param[in] game    ID of the game being loaded.
 * \param[in] prg_fn  Filename for the merged program ROM.
 *
 * \return TRUE if successful, FALSE otherwise.
 */
BOOL spi_load_data(SPIGameID game, const char *prg_fn);

/*! \brief Execute a single frame.
 */
void spi_execute_frame(void);

#endif
