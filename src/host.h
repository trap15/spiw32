/* SPIW32 - SPI for Windows 95
 *
 * Copyright (C) 2018 Alex "trap15" Marshall <trap15@raidenii.net>
 * All rights reserved. No warranty implied.
 */

/*! \file
 *
 * \brief Functions relating to the host machine.
 */

#ifndef HOST_H_
#define HOST_H_

/*! \brief Gets the current CS selector.
 *
 * \return The current CS selector.
 */
static inline WORD cpu_cs(void) { asm mov ax, cs }

/*! \brief Gets the current DS selector.
 *
 * \return The current DS selector.
 */
static inline WORD cpu_ds(void) { asm mov ax, ds }

/*! \brief Flag that is set true when running on Win9x OS.
 */
extern BOOL g_host_win9x;

/*! \brief Flag that is set true when running on 64-bit Windows.
 */
extern BOOL g_host_win64;

/*! \brief Setup stuff.
 */
void host_setup(void);

#endif
