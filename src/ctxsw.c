/* SPIW32 - SPI for Windows 95
 *
 * Copyright (C) 2018 Alex "trap15" Marshall <trap15@raidenii.net>
 * All rights reserved. No warranty implied.
 */

/*! \file
 *
 * \brief Context switch functionality.
 */

#include "top.h"

#include "ctxsw.h"
#include "host.h"
#include "ldt.h"
#include "spi_state.h"

/*! \brief Initialize the context switching code.
 *
 * Returns TRUE on success, FALSE on failure.
 */
BOOL ctxsw_init(void) {
  // Write a large ptr to the callshim.
  WR32(SPIHOLE_CALLSHIM_OFFSET+0, (DWORD)ctxsw_hostcall);
  WR16(SPIHOLE_CALLSHIM_OFFSET+4, cpu_cs());
}

/*! \brief Initialize a context.
 *
 * \param[inout]  ctx   Context to initialize.
 */
void ctxsw_ctx_init(SPIExecCtx *ctx) {
  memset(ctx, 0, sizeof(ctx));
  ctx->r_cs = g_ldt_cs;
  ctx->r_ds = g_ldt_ds;
  ctx->r_es = g_ldt_ds;
  ctx->r_fs = g_ldt_ds;
  ctx->r_gs = g_ldt_ds;
  ctx->r_ss = g_ldt_ds;
}

/*! \brief Set the stack for a context.
 *
 * \param[inout]  ctx   Context to assign stack to.
 * \param[in]     size  Number of bytes in the stack.
 * \param[inout]  ptr   Pointer to the base of the buffer to use. This is in host memory space.
 *                      If this is NULL, a buffer will be allocated. This _must_
 *                      be manually freed!
 */
void ctxsw_ctx_stack_set(SPIExecCtx *ctx, DWORD size, void *ptr) {
  if(ptr == NULL) {
    ptr = malloc(size);
  }
  ctx->r_esp = REAL2EMU(ptr) + size;
}

/*! \brief Push one DWORD to a context's stack.
 *
 * \param[inout]  ctx     Context containing the stack to push to.
 * \param[in]     value   Value to push.
 */
void ctxsw_ctx_push(SPIExecCtx *ctx, DWORD value) {
  DWORD *stack = (DWORD*)EMU2REAL(ctx->r_esp);
  *(--stack) = value;
  ctx->r_esp = REAL2EMU(stack);
}
