/* SPIW32 - SPI for Windows 95
 *
 * Copyright (C) 2018 Alex "trap15" Marshall <trap15@raidenii.net>
 * All rights reserved. No warranty implied.
 */

/*! \file
 *
 * \brief Functions relating to the host machine.
 */

#include "top.h"

#include "host.h"
#include "ldt.h"

/*! \brief Flag that is set true when running on Win9x OS.
 */
BOOL g_host_win9x;

/*! \brief Flag that is set true when running on 64-bit Windows.
 */
BOOL g_host_win64;

/*! \brief Setup stuff.
 */
void host_setup(void) {
  DWORD dwVersion = GetVersion();

  DBG("version %08X\n", dwVersion);
  g_host_win9x = (dwVersion & 0xFF) <= 4;
  g_host_win64 = FALSE;

  if(!g_host_win9x) {
    *(FARPROC*)(&NtSetLdtEntries) = GetProcAddress(LoadLibrary("ntdll.dll"), "NtSetLdtEntries");
    LDTEntry ldt_empty = {0};
    DWORD status = NtSetLdtEntries(~0, ldt_empty, ~0, ldt_empty);
    if(status == 0xC0000002) {
      DBG("NtSetLdtEntries is NOT_IMPLEMENTED, we must be win64.\n");
      g_host_win64 = TRUE;
    }
  }
}
