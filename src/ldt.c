/* SPIW32 - SPI for Windows 95
 *
 * Copyright (C) 2018 Alex "trap15" Marshall <trap15@raidenii.net>
 * All rights reserved. No warranty implied.
 */

/*! \file
 *
 * \brief LDT handling functions.
 */

#include "top.h"

#include "host.h"
#include "ldt.h"

DWORD (__stdcall *NtSetLdtEntries)(ULONG Selector1, LDTEntry LdtEntry1,
                         ULONG Selector2, LDTEntry LdtEntry2);

/*! \brief Pointer to the LDT table.
 *
 * \note I have zero clue if this holds beyond my specific win98se install.
 */
#define LDTPtr ((LDTEntry*)0x80004000)

/*! \brief Number of entries in the LDT table.
 *
 * \note This is a spitball, I need to find address of LDTAlias.
 */
#define LDTAlias (0x100)

/*! \brief Number of reserved LDT entries at the beginning of the table.
 *
 * \note Just a guess.
 */
#define LDT_RESERVED_ENTRY_COUNT (0x1)

//! \brief Emulated CS segment selector.
WORD g_ldt_cs;
//! \brief Emulated DS segment selector.
WORD g_ldt_ds;
//! \brief Offset to add to an emulated segment offset to get a regular offset.
DWORD g_ldt_offset;

/*! \brief Debug print contents of an LDT entry.
 *
 * \param[in] ldt   Buffer containing the LDT entry to print.
 */
static void _ldt_print(const LDTEntry *ldt) {
  DWORD base = ldt->base_lo | (ldt->base_mid << 16) | (ldt->base_hi << 24);
  DWORD limit = ldt->limit_lo | ((ldt->flags__limit_hi & 0xF) << 16);
  BYTE access = ldt->access;
  BYTE flags = ldt->flags__limit_hi >> 4;
  DBG("%08X: 0x%08X | 0x%05X | 0x%02X | 0x%01X |\n", (DWORD)ldt, base,
      limit, access, flags);
}

/*! \brief Check whether an LDT entry is free.
 *
 * \param[in] ldt   Buffer containing the LDT entry to check.
 *
 * \return TRUE if the LDT entry is free.
 */
static BOOL _ldt_is_free(const LDTEntry *ldt) {
  return (ldt->access & 0x90) != 0x90;
}

/*! \brief Builds an LDT entry from its parts.
 *
 * See GDT structure documents for information about why this is necessary...
 *
 * \param[out]  ldt     Buffer to fill with the entry data.
 * \param[in]   base    Base address of the region.
 * \param[in]   limit   Limit value for the region.
 * \param[in]   access  Access flags.
 * \param[in]   flags   Various flags.
 */
static void _ldt_build(LDTEntry *ldt, DWORD base, DWORD limit, BYTE access,
                       BYTE flags) {
  limit--;
  ldt->limit_lo = limit & 0xFFFF;
  ldt->base_lo = base & 0xFFFF;
  ldt->base_mid = (base >> 16) & 0xFF;
  ldt->access = access;
  ldt->flags__limit_hi = (flags << 4) | ((limit >> 16) & 0xF);
  ldt->base_hi = (base >> 24) & 0xFF;
}

/*! \brief Sets up the required LDTs.
 *
 * This should be called at startup.
 *
 * \param[in] base  Base address of the memory region to use.
 * \param[in] size  Size in bytes of the memory region to use.
 */
BOOL ldt_setup(void *base, DWORD size) {
  LDTEntry *ldt = LDTPtr;
  LDTEntry ent_cs, ent_ds;

  // Build the LDT entries up front.
  BYTE flags = 0xC;
  g_ldt_offset = -(DWORD)base;
  // By setting a limit of 0 with Gr set, we are just defining
  // a "shifted" aperture
  _ldt_build(&ent_cs, (DWORD)base, 0, 0xFA, 0xC);
  _ldt_build(&ent_ds, (DWORD)base, 0, 0xF2, 0xC);

  g_ldt_cs = g_ldt_ds = 0;

  // Find two LDT entries that are free to use for CS and DS.
  if(g_host_win9x) {
    for(int i = LDT_RESERVED_ENTRY_COUNT; i < LDTAlias; i++) {
      if(_ldt_is_free(&ldt[i])) {
        if(g_ldt_cs == 0) {
          g_ldt_cs = i;
        }else{
          g_ldt_ds = i;
          break;
        }
      }
    }

    if((g_ldt_cs == 0) || (g_ldt_ds == 0)) {
      DBG("LDT setup couldn't find 2 free descriptors!\n");
      goto _ldt_setup_failed;
    }

    ldt[g_ldt_cs] = ent_cs;
    ldt[g_ldt_ds] = ent_ds;

    // Fixup the selectors to actually be selectors.
    g_ldt_cs = (g_ldt_cs << 3) | 7;
    g_ldt_ds = (g_ldt_ds << 3) | 7;
  }else if(g_host_win64) { // 64-bit WinNT
    DBG("Sorry, this isn't possible on Win64 yet ;_;\n");
    goto _ldt_setup_failed;
  }else{ // 32-bit WinNT
    // I hope these are free lmao
    g_ldt_cs = 0x87;
    g_ldt_ds = 0x8F;
    DWORD status = NtSetLdtEntries(g_ldt_cs, ent_cs, g_ldt_ds, ent_ds);
    if(status != 0) {
      DBG("NT LDT setup failed %08X\n", status);
    }
  }

  // Validate that the DS selector works
  DWORD testvalA, testvalB;
  asm {
    mov ax, g_ldt_ds
    mov gs, ax
    mov ebx, base
    // Write 0xFEEDFACE to the buffer through regular aperture
    mov dword ptr [ebx], 0FEEDFACEh
    // Read it back through new DS
    mov eax, gs:[0]
    // Save to testvalA
    mov testvalA, eax
    // Write 0x0BADDAD5 to the buffer through the new DS
    mov dword ptr gs:[0], 0BADDAD5h
    // And read it back through the regular aperture
    mov eax, [ebx]
    // Save to testvalB
    mov testvalB, eax
  }
  if(testvalA != 0xFEEDFACE) {
    DBG("DS testvalA failed\n");
    goto _ldt_setup_failed;
  }
  if(testvalB != 0x0BADDAD5) {
    DBG("DS testvalB failed\n");
    goto _ldt_setup_failed;
  }

  // Validate that the CS selector works
  // Copy in some shellcode (mov eax,0xA5A5C3C3 ; xor eax,ecx ; retf)
  // Unfortunately it has to also include a far pointer to its own shellcode,
  // because I can't figure this crap out.
  memcpy(base, "\x06\x00\x00\x00\x00\x00\xB8\xC3\xC3\xA5\xA5\x31\xC8\xCB", 14);
  __asm {
    xor eax, eax
    mov ax, g_ldt_cs
    mov gs, ax
    mov ebx, [base]
    // Write the new CS selector to our far pointer in the test area.
    mov 4[ebx], ax
    // Write some magic testing number
    mov ecx, 012345678h
    // Execute the instructions
    // call far gs:0
    db 065h, 0FFh, 01Dh, 0,0,0,0
    // Gather results
    mov testvalA, eax
    mov testvalB, ecx
  }
  DWORD expect_testvalA = (0xA5A5C3C3 ^ 0x12345678);
  if(testvalA != expect_testvalA) {
    DBG("CS testvalA failed (%08X vs %08X)\n", testvalA, expect_testvalA);
    goto _ldt_setup_failed;
  }
  if(testvalB != 0x12345678) {
    DBG("CS testvalB failed (%08X vs %08X)\n", testvalB, 0x12345678);
    goto _ldt_setup_failed;
  }

  return TRUE;

_ldt_setup_failed:
  ldt_cleanup();
  g_ldt_offset = 0;
  return FALSE;
}

/*! \brief Cleans up LDTs since Windows won't do it for us I guess.
 */
void ldt_cleanup(void) {
  if(g_host_win9x) {
    LDTEntry *ldt = LDTPtr;
    if(g_ldt_cs) {
      ldt[g_ldt_cs >> 3].access = 0;
      g_ldt_cs = 0;
    }
    if(g_ldt_ds) {
      ldt[g_ldt_ds >> 3].access = 0;
      g_ldt_ds = 0;
    }
  }
}
