/* SPIW32 - SPI for Windows 95
 *
 * Copyright (C) 2018 Alex "trap15" Marshall <trap15@raidenii.net>
 * All rights reserved. No warranty implied.
 */

/*! \file
 *
 * \brief LDT handling functions.
 */

#ifndef LDT_H_
#define LDT_H_

//! \brief LDT entry structure.
typedef struct LDTEntry {
  WORD limit_lo;
  WORD base_lo;
  BYTE base_mid;
  BYTE access;
  BYTE flags__limit_hi;
  BYTE base_hi;
} LDTEntry;

/*! \brief Sets up the required LDTs.
 *
 * This should be called at startup.
 *
 * \param[in] base  Base address of the memory region to use.
 * \param[in] size  Size in bytes of the memory region to use.
 *
 * \return TRUE if successful, FALSE otherwise.
 */
BOOL ldt_setup(void *base, DWORD size);

/*! \brief Cleans up LDTs since Windows won't do it for us I guess.
 */
void ldt_cleanup(void);

//! \brief Emulated CS segment selector.
extern WORD g_ldt_cs;

//! \brief Emulated DS segment selector.
extern WORD g_ldt_ds;

//! \brief Offset to add to an emulated segment offset to get a regular offset.
extern DWORD g_ldt_offset;

#define EMU2REAL(ADDR) ((DWORD)(ADDR) + g_ldt_offset)
#define REAL2EMU(ADDR) ((DWORD)(ADDR) - g_ldt_offset)

extern DWORD (__stdcall *NtSetLdtEntries)(ULONG Selector1, LDTEntry LdtEntry1,
                                ULONG Selector2, LDTEntry LdtEntry2);

#endif
